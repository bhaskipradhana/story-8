from django.shortcuts import render
from django.http import JsonResponse
import requests

def index(request):
    return render(request, 'story8.html')

def get_data(request):
    key = request.GET['key']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + key
    response = requests.get(url)
    response_json = response.json()

    return JsonResponse(response_json)


# Create your views here.
